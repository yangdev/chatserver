<?php

namespace App\WebSocketsServer;

use SplObjectStorage;
use Illuminate\Support\Facades\Redis;
use Ratchet\ConnectionInterface;
use Ratchet\RFC6455\Messaging\MessageInterface;
use Ratchet\WebSocket\MessageComponentInterface;


class ChatServiceWebSocketHandler implements MessageComponentInterface
{
    /**
     * 客戶端陣列
     *
     * @var [type]
     */
    private $clients = [];

    private $type_arr = ['football', 'basketball', 'lol', 'dota2', 'csgo'];

    public function __construct()
    {
        // 建立5間房的SplObjectStorage
        // $this->clients['football'] = new SplObjectStorage();
        // $this->clients['basketball'] = new SplObjectStorage();
        // $this->clients['lol'] = new SplObjectStorage();
        // $this->clients['dota2'] = new SplObjectStorage();
        // $this->clients['csgo'] = new SplObjectStorage();
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->buildChatRoom($conn);
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->clients[$conn->tpye][$conn->room]->detach($conn);
        dump($this->clients[$conn->tpye][$conn->room]->count());
        dump('close');
        // $conn->close();
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        // TODO: Implement onError() method.
    }

    public function onMessage(ConnectionInterface $conn, MessageInterface $msg)
    {
        $room = $this->clients[$conn->tpye][$conn->room];
        foreach ($room as $client) {
            if ($conn !== $client) {
                $client->send($msg);
            }
        }
    }

    /**
     * 是否建立新的賽事房間, 並將連線歸屬
     * 
     * @param \Ratchet\ConnectionInterface $conn
     * @return void
     */
    private function buildChatRoom(ConnectionInterface $conn)
    {
        try {
            $querry_arr = [];
            $query = $conn->httpRequest->getUri()->getQuery();
            parse_str($query, $querry_arr);
            if ((!isset($querry_arr['type'])) || (!isset($querry_arr['room']))) {
                echo ('參數錯誤');
                $conn->close();
            }
            if (!in_array($querry_arr['type'], $this->type_arr)) {
                echo ('聊天室型態有誤');
                $conn->close();
            }
            $conn->tpye = $querry_arr['type'];
            $conn->room = $querry_arr['room'];
            // 測試房間是否存在
            if (!isset($this->clients[$conn->tpye][$conn->room])) {
                $this->clients[$conn->tpye][$conn->room] = new SplObjectStorage();
            }
            $this->clients[$conn->tpye][$conn->room]->attach($conn);
            dump($this->clients[$conn->tpye][$conn->room]->count());
        } catch (\Throwable $th) {
            dd($th);
        }
    }
}
