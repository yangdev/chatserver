<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use React\EventLoop\Loop;
use React\Socket\SocketServer;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use App\WebSocketsServer\{ChatServiceWebSocketHandler};
use Exception;

/**
 * Websocket Server 啟動服務 (React\Socket\SocketServer)
 * {method} 啟動類型
 * {--host} 限定host
 * {--port} port號
 */


class WebsocketServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ws {method} {--host=0.0.0.0} {--port=9090}';

    /**
     * Start the WebSocket Server.
     *
     * @var string
     */
    protected $description = 'Start the WebSocket Server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $method = $this->argument('method');
            switch ($method) {
                case 'chat-server':
                    $service = new ChatServiceWebSocketHandler();
                    break;
                
                default:
                    $service = new ChatServiceWebSocketHandler();
                    // throw new Exception("method not found", 1);
                    break;
            }
            $port = intval($this->option('port'));
            $host = $this->option('host');
            $loop = Loop::get();
            $websock = new SocketServer("$host:$port", [], $loop);
            $webServer = new IoServer(
                new HttpServer(
                    new WsServer(
                        $service
                    )
                ),
                $websock
            );
            echo "啟動websocket method=$method, port=$port, host=$host";
            $loop->run();
        } catch (\Throwable $th) {
            dd($th);
            //throw $th;
        }
    }
}
